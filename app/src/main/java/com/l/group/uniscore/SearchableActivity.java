package com.l.group.uniscore;

import android.app.Activity;
import android.app.ListActivity;
import android.app.SearchManager;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.VolleyError;

import java.util.ArrayList;
import java.util.List;

public class SearchableActivity extends AppCompatActivity {

    UniScoreSDK sdk = new UniScoreSDK(this);

    List<User> users = new ArrayList<User>();
    List<Game> games  = new ArrayList<Game>();
    SwipeRefreshLayout refreshLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        Intent intent = getIntent();
        refreshLayout = findViewById(R.id.swipeRefresh);
        if(Intent.ACTION_SEARCH == intent.getAction()) {
            final String query = intent.getStringExtra(SearchManager.QUERY);
            refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {
                    ExecuteSearch(query);
                }
            });
            ExecuteSearch(query);
        }
    }

    void PopulateList (final List<User> users, final List<Game> games, final String type) {
        findViewById(R.id.progressBar).setVisibility(View.VISIBLE);
        ListView listView = (ListView)findViewById(R.id.list);
        listView.setAdapter(new SearchListAdapter(users, games,SearchableActivity.this, type));
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String gamesString = getString(R.string.games);
                String usersString = getString(R.string.users);
                if(type.equals(getString(R.string.users))) {
                    Intent intent = new Intent(SearchableActivity.this, EmptyActivity.class);
                    intent.putExtra("TYPE", "Users");
                    intent.putExtra("id",users.get(position).id);
                    startActivity(intent);
                } else if (type.equals(getString(R.string.games))) {
                    Intent intent = new Intent(SearchableActivity.this, EmptyActivity.class);
                    intent.putExtra("TYPE", "Games");
                    intent.putExtra("id", String.valueOf(games.get(position).id));
                    startActivity(intent);
                }
            }
        });
        findViewById(R.id.progressBar).setVisibility(View.GONE);
    }

    private void ExecuteSearch(final String query) {
        sdk.GetUsers(query + "%", new ResponseListener() {
            @Override
            public void OnError(VolleyError error) {
                Toast toast = Toast.makeText(SearchableActivity.this, R.string.Failure, Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.TOP, 0,40);
                toast.show();
            }

            @Override
            public void OnResponse(Object response) {
                users = (List<User>) response;
                sdk.GetGames(query + "%", new ResponseListener() {
                    @Override
                    public void OnError(VolleyError error) {
                        Toast toast = Toast.makeText(SearchableActivity.this, R.string.Failure, Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.TOP, 0,40);
                        toast.show();
                    }

                    @Override
                    public void OnResponse(Object response) {
                        games = (List<Game>) response;
                        TabLayout tabs = findViewById(R.id.SearchTabs);
                        tabs.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
                            @Override
                            public void onTabSelected(TabLayout.Tab tab) {
                                PopulateList(users, games, tab.getText().toString());
                            }

                            @Override
                            public void onTabUnselected(TabLayout.Tab tab) {

                            }

                            @Override
                            public void onTabReselected(TabLayout.Tab tab) {

                            }
                        });

                        PopulateList(users, games, getString(R.string.users));
                        refreshLayout.setRefreshing(false);
                    }
                });
            }
        });
    }
}
