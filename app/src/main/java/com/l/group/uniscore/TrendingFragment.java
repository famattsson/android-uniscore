package com.l.group.uniscore;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.Toast;

import com.android.volley.VolleyError;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class TrendingFragment extends Fragment {

    UniScoreSDK sdk = new UniScoreSDK(getActivity());
    List<Game> trendingGames = new ArrayList<>();
    GridView gridView;
    View view;
    private SwipeRefreshLayout refreshLayout;

    public TrendingFragment() {
        // Required empty public constructor
    }

    private void GetData () {
        sdk.GetGames("%%", new ResponseListener() {
            @Override
            public void OnError(VolleyError error) {
                Toast toast = Toast.makeText(getContext(), R.string.Failure, Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.TOP, 0,40);
                toast.show();
            }
            @Override
            public void OnResponse(Object response) {
                if(getContext() != null) {
                    final List<Game> games = (List<Game>)response;
                    Collections.sort(games, new Comparator<Game>() {
                        @Override
                        public int compare(Game o1, Game o2) {
                            return Integer.compare(o2.scores.size(), o1.scores.size());
                        }
                    });
                    trendingGames = games;
                    gridView = view.findViewById(R.id.trendingGrid);
                    gridView.setAdapter(new TrendingAdapter(games, getContext()));
                    gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            Intent intent = new Intent(getContext(), EmptyActivity.class);
                            intent.putExtra("TYPE", "Games");
                            intent.putExtra("id", String.valueOf(games.get(position).id));
                            startActivity(intent);
                        }
                    });
                    view.findViewById(R.id.progressBar).setVisibility(View.GONE);
                    refreshLayout.setRefreshing(false);
                }
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ReqQ.getInstance(getContext()).getRequestQueue().cancelAll("Games");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_trending, container, false);
        refreshLayout = view.findViewById(R.id.swipeRefresh);
        refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                GetData();
            }
        });
        GetData();
        return view;
    }


}
