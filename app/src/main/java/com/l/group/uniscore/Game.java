package com.l.group.uniscore;

import android.graphics.Bitmap;

import java.util.List;

public class Game {
    public int id;
    public String name, releaseDate, developerName, genreName, publisherName;
    public List<Score> scores;
    public Bitmap profilePic;

    @Override
    public boolean equals(Object v) {
        boolean retVal = false;

        if (v instanceof Game){

            retVal = ((Game) v).id == this.id;
        }

        return retVal;
    }

    public Game(List<Score> scores, int id, String name, String releaseDete, String developerName, String genreName, String publisherName, Bitmap profilePic) {
        this.scores = scores;
        this.id = id;
        this.name = name;
        this.releaseDate = releaseDete;
        this.developerName = developerName;
        this.genreName = genreName;
        this.publisherName = publisherName;
        this.profilePic = profilePic;
    }
}
