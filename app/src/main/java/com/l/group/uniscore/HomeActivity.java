package com.l.group.uniscore;

import android.app.SearchManager;
import android.content.ComponentName;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.SearchView;

public class HomeActivity extends AppCompatActivity {

    BottomNavigationView navigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        ((SearchView)findViewById(R.id.searchBar)).setSearchableInfo(((SearchManager)getSystemService(Context.SEARCH_SERVICE)).getSearchableInfo(new ComponentName(this, SearchableActivity.class)));


        if(navigationView == null) {
            navigationView = findViewById(R.id.bottomNavBar);

            final UserFragment usersFragment = new UserFragment();
            final TrendingFragment trendingFragment = new TrendingFragment();
            final SettingsFragment settingsFragment = new SettingsFragment();
            final GameFragment gameFragment = new GameFragment();

            navigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
                @Override
                public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {

                    Bundle userArgs = new Bundle();
                    userArgs.putSerializable("userId",LoggedInUser.getInstance().id);
                    usersFragment.setArguments(userArgs);
                    Bundle gameArgs = new Bundle();
                    gameArgs.putSerializable("gameId",LoggedInUser.getInstance().id);
                    gameFragment.setArguments(gameArgs);

                    int id = menuItem.getItemId();
                    if (id == R.id.trending) {
                        setFragment(trendingFragment);
                        return true;
                    } else if (id == R.id.users){
                        if(LoggedInUser.getInstance().isDev)
                            setFragment(gameFragment);
                        else
                            setFragment(usersFragment);
                        return true;
                    } else if (id == R.id.settings) {
                        setFragment(settingsFragment);
                        return true;
                    }
                    return false;
                }
            });

            navigationView.setSelectedItemId(R.id.trending);
        }

    }

    private void setFragment(Fragment fragment) {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.frame, fragment);
        fragmentTransaction.commit();
    }
}
