package com.l.group.uniscore;

import java.util.ArrayList;
import java.util.List;

class LoadedResources {
    private static final LoadedResources ourInstance = new LoadedResources();
    public List<User> users = new ArrayList<>();
    public List<Game> games = new ArrayList<>();
    public List<Score> scores = new ArrayList<>();
    public List<FollowerRelation> followerRelations = new ArrayList<>();

    static LoadedResources getInstance() {
        return ourInstance;
    }

    private LoadedResources() {
    }
}
