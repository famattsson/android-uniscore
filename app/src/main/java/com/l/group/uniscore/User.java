package com.l.group.uniscore;

import android.graphics.Bitmap;

import java.util.List;

public class User {
    public String id, username, email;
    public List<Score> scores;
    public Bitmap profilePic;

    @Override
    public boolean equals(Object v) {
        boolean retVal = false;

        if (v instanceof User){

            retVal = ((User) v).id.equals(this.id);
        }

        return retVal;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 17 * hash + (this.id != null ? this.id.hashCode() : 0);
        return hash;
    }

    public User (String id, String username, String email, List<Score> scores, Bitmap profilePic) {
        this.id = id;
        this.username = username;
        this.email = email;
        this.scores = scores;
        this.profilePic = profilePic;
    }
}
