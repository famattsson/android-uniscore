package com.l.group.uniscore;

import com.android.volley.VolleyError;

public interface ResponseListener {
    void OnError (VolleyError error);

    void OnResponse (Object response);
}
