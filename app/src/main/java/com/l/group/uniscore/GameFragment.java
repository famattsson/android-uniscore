package com.l.group.uniscore;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;

public class GameFragment extends Fragment {

    UniScoreSDK sdk;
    public Game game;
    public GridView list;
    public TabLayout tabLayout;
    public View view;
    SwipeRefreshLayout refreshLayout;
    private TabLayout tabs;
    private List<Score> gameScores;
    private List<Score> followingScores;

    public GameFragment() {
    }

    void Populate (final List<Score> scores) {

        view.findViewById(R.id.progressBar).setVisibility(View.VISIBLE);
        view.findViewById(R.id.progressBar2).setVisibility(View.VISIBLE);
        list.setAdapter(new GameListAdapter(scores, Objects.requireNonNull(getContext())));
        ((TextView)view.findViewById(R.id.TopBarInfo)).setText(game.name);

        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(getContext(), EmptyActivity.class);
                intent.putExtra("TYPE", "Users");
                intent.putExtra("id", scores.get(position).user.id);
                startActivity(intent);
            }
        });
        view.findViewById(R.id.progressBar).setVisibility(View.GONE);
        view.findViewById(R.id.progressBar2).setVisibility(View.GONE);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ReqQ.getInstance(getContext()).getRequestQueue().cancelAll("Game");
    }

    private void GetData () {
        sdk.GetGame(getArguments().getString("gameId"), new ResponseListener() {
            @Override
            public void OnError(VolleyError error) {
                Toast toast = Toast.makeText(getContext(), R.string.Failure, Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.TOP, 0,40);
                toast.show();
            }

            @Override
            public void OnResponse(Object response) {
                if(getContext() != null) {
                    final List<Game> games = (List<Game>) response;
                    game = games.get(0);
                    if(game.scores.size() == 0) {
                        TextView textView = new TextView(getContext());
                        textView.setText(getString(R.string.GameScreenNoScores));
                        ((LinearLayout) view.findViewById(R.id.UserLayout)).addView(textView, 2);
                        Populate(new ArrayList<Score>());
                    }
                    gameScores = game.scores;

                    sdk.GetFollowing(String.valueOf(LoggedInUser.getInstance().id), new ResponseListener() {
                        @Override
                        public void OnError(VolleyError error) {
                            Toast toast = Toast.makeText(getContext(), R.string.Failure, Toast.LENGTH_SHORT);
                            toast.setGravity(Gravity.TOP, 0,40);
                            toast.show();
                        }

                        @Override
                        public void OnResponse(Object response) {
                            if(getContext() != null) {
                                final ImageView profilePicView = view.findViewById(R.id.TopBarImage);
                                if(game.profilePic == null) {
                                    profilePicView.setImageResource(R.drawable.game_icon);
                                } else {
                                    profilePicView.setImageBitmap(game.profilePic);
                                }

                                followingScores = new ArrayList<>();
                                for (User user : ((List<User>) response)) {
                                    for (Score score : user.scores) {
                                        score.user = user;
                                        followingScores.add(score);
                                    }
                                }
                                followingScores.retainAll(gameScores);
                                Collections.sort(followingScores, new Comparator<Score>() {
                                    @Override
                                    public int compare(Score o1, Score o2) {
                                        return Integer.compare(o2.score, o1.score);
                                    }
                                });
                                Collections.sort(gameScores, new Comparator<Score>() {
                                    @Override
                                    public int compare(Score o1, Score o2) {
                                        return Integer.compare(o2.score, o1.score);
                                    }
                                });
                                for (Score score : gameScores) {
                                    if(LoggedInUser.getInstance().id == score.user.id) {
                                        gameScores.remove(score);
                                        gameScores.add(0, score);
                                        break;
                                    }

                                }

                                String type = tabs.getTabAt(tabs.getSelectedTabPosition()).getText().toString();
                                if(type.equals(getString(R.string.GameFragmentGlobalTab))) {
                                    Populate(gameScores);
                                } else if (type.equals(getString(R.string.following))) {
                                    Populate(followingScores);
                                }
                                refreshLayout.setRefreshing(false);
                            }
                        }
                    });
                }
            }
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_users, container, false);
        sdk = new UniScoreSDK(getContext());
        list = view.findViewById(R.id.UserList);
        refreshLayout = view.findViewById(R.id.swipeRefresh);
        tabs = view.findViewById(R.id.UserTabs);
        tabs.removeAllTabs();

        tabs.addTab(tabs.newTab().setText(R.string.GameFragmentGlobalTab));
        tabs.addTab(tabs.newTab().setText(R.string.following));

        tabs.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                String type = tab.getText().toString();
                if(type.equals(getString(R.string.GameFragmentGlobalTab))) {
                    Populate(gameScores);
                } else if (type.equals(getString(R.string.following))) {
                    Populate(followingScores);
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                GetData();
            }
        });

        GetData();

        return view;
    }

}
