package com.l.group.uniscore;

public class Score {
    public int id, userId, gameId, score;

    public Game game;
    public User user;

    @Override
    public boolean equals(Object v) {
        boolean retVal = false;

        if (v instanceof Score){

            retVal = ((Score) v).id == this.id;
        }

        return retVal;
    }


    public Score(int id, int userId, int gameId, int score, Game game, User user) {
        this.id = id;
        this.userId = userId;
        this.gameId = gameId;
        this.score = score;
        this.game = game;
        this.user = user;
    }
}
