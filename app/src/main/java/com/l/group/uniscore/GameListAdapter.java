package com.l.group.uniscore;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.design.widget.FloatingActionButton;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.VolleyError;

import java.util.List;

public class GameListAdapter extends BaseAdapter {
    private List<Score> scores;
    private LayoutInflater layoutInflater;
    private UniScoreSDK sdk;
    private Context mContext;

    public GameListAdapter(List<Score> scores, Context mContext) {
        this.scores = scores;
        this.mContext = mContext;
        this.layoutInflater = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        sdk = new UniScoreSDK(mContext);
    }

    @Override
    public int getCount() {
        return scores.size();
    }

    @Override
    public Object getItem(int position) {
        return scores.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    private void SetFollowButtonAction(final FloatingActionButton actionButton, final int position) {
        sdk.GetFollowingIds(String.valueOf(LoggedInUser.getInstance().id), new ResponseListener() {
            @Override
            public void OnError(VolleyError error) {

            }

            @Override
            public void OnResponse(Object response) {
                List<User> following = (List<User>) response;
                if(following.contains(scores.get(position).user)) {
                    actionButton.setImageResource(R.drawable.iconfinder_minus_216340);
                    actionButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            sdk.DeleteFollower(scores.get(position).user.id, new ResponseListener() {
                                @Override
                                public void OnError(VolleyError error) {

                                }

                                @Override
                                public void OnResponse(Object response) {
                                    SetFollowButtonAction(actionButton, position);
                                }
                            });
                        }
                    });
                } else {
                    actionButton.setImageResource(R.drawable.plus);
                    actionButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            sdk.PostFollower(scores.get(position).user, new ResponseListener() {
                                @Override
                                public void OnError(VolleyError error) {

                                }

                                @Override
                                public void OnResponse(Object response) {
                                    SetFollowButtonAction(actionButton, position);
                                }
                            });
                        }
                    });
                }
            }
        });
    }

    @Override
    public View getView(final int position, View convertView, final ViewGroup parent) {

        if(convertView == null) {
            convertView =  layoutInflater.inflate(R.layout.table_cell, parent, false);
        }

        TextView name = convertView.findViewById(R.id.TableCellInfo1);
        TextView scoreText = convertView.findViewById(R.id.TableCellInfo2);
        final FloatingActionButton actionButton = convertView.findViewById(R.id.floatingActionButton);
        ImageView imageView = convertView.findViewById(R.id.TableCellImage);
        name.setText(scores.get(position).user.username);
        scoreText.setText(String.valueOf(scores.get(position).score));
        if(scores.get(position).user.profilePic == null) {
            imageView.setImageResource(R.drawable.ic_person_black_24dp);
        } else {
            imageView.setImageBitmap(scores.get(position).user.profilePic);
        }
        if(LoggedInUser.getInstance().id != null) {
            if(LoggedInUser.getInstance().id.equals(String.valueOf(scores.get(position).gameId)) || scores.get(position).user.id.equals(LoggedInUser.getInstance().id)) {
                actionButton.show();
                actionButton.setImageResource(R.drawable.iconfinder_close_icon_1398919);
                actionButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                        builder.setTitle(mContext.getString(R.string.ScoreDeleteConfirm));
                        builder.setPositiveButton(R.string.Confirm, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                sdk.DeleteScore(String.valueOf(scores.get(position).id),new ResponseListener() {
                                    @Override
                                    public void OnError(VolleyError error) {

                                    }

                                    @Override
                                    public void OnResponse(Object response) {
                                        parent.invalidate();
                                    }
                                });
                            }
                        });
                        builder.setNegativeButton(R.string.No, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                            }
                        });
                        builder.show();
                    }
                });
            } else if (!LoggedInUser.getInstance().isDev) {
                actionButton.show();
                SetFollowButtonAction(actionButton, position);
            }
        } else {
            actionButton.hide();
        }


        return convertView;
    }
}
