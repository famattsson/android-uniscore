package com.l.group.uniscore;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

public class TrendingAdapter extends BaseAdapter {

    List<Game> games;
    private LayoutInflater layoutInflater;
    Context mContext;

    public TrendingAdapter(List<Game> games, Context mContext) {
        this.games = games;
        this.layoutInflater = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);;
        this.mContext = mContext;
    }

    @Override
    public int getCount() {
        return games.size();
    }

    @Override
    public Object getItem(int position) {
        return games.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if(convertView == null) {
            convertView =  layoutInflater.inflate(R.layout.trending_cell, parent, false);
        }

        TextView GameName = convertView.findViewById(R.id.GameNameGrid);
        GameName.setText(games.get(position).name);
        if(games.get(position).profilePic != null) {
            ((ImageView)convertView.findViewById(R.id.TrendingCellImage)).setImageBitmap(games.get(position).profilePic);
        } else {
            ((ImageView)convertView.findViewById(R.id.TrendingCellImage)).setImageResource(R.drawable.game_icon);
        }

        return convertView;
    }
}
