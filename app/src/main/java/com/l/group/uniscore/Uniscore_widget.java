package com.l.group.uniscore;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.RemoteViews;
import android.widget.Toast;

import com.android.volley.VolleyError;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class Uniscore_widget extends AppWidgetProvider {

    @Override
    public void onUpdate(final Context context, final AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        for (final int appWidgetId : appWidgetIds) {
            final RemoteViews views = new RemoteViews(context.getPackageName(), R.layout.uniscore_widget);
            final Intent intent = new Intent(context, EmptyActivity.class);
            views.setViewVisibility(R.id.WidgetProgressbar, View.VISIBLE);
            intent.putExtra("TYPE", "Games");
            context.getApplicationContext();
            UniScoreSDK sdk = new UniScoreSDK(context);
            sdk.GetGames("%%", new ResponseListener() {
                @Override
                public void OnError(VolleyError error) {
                    Toast toast = Toast.makeText(context, R.string.Failure, Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.TOP, 0,40);
                    toast.show();
                }
                @Override
                public void OnResponse(Object response) {
                    final List<Game> games = (List<Game>)response;
                    Collections.sort(games, new Comparator<Game>() {
                        @Override
                        public int compare(Game o1, Game o2) {
                            return Integer.compare(o2.scores.size(), o1.scores.size());
                        }
                    });
                    intent.putExtra("id", String.valueOf(games.get(0).id));
                    intent.setData(Uri.withAppendedPath(Uri.parse(String.valueOf(appWidgetId)), String.valueOf(appWidgetId)));
                    PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent, 0);
                    views.setTextViewText(R.id.WidgetTextView, games.get(0).name);
                    views.setOnClickPendingIntent(R.id.WidgetImageVariant, pendingIntent);
                    if(games.get(0).profilePic != null) {
                        views.setBitmap(R.id.WidgetImageVariant, "setImageBitmap", games.get(0).profilePic);
                    }
                    Intent intentSync = new Intent(context, Uniscore_widget.class);
                    intentSync.setAction(AppWidgetManager.ACTION_APPWIDGET_UPDATE);
                    intentSync.putExtra( AppWidgetManager.EXTRA_APPWIDGET_IDS, new int[] { appWidgetId } );
                    PendingIntent pendingSync = PendingIntent.getBroadcast(context,0, intentSync, PendingIntent.FLAG_UPDATE_CURRENT);
                    views.setOnClickPendingIntent(R.id.WidgetRefreshBtn, pendingSync);
                    views.setViewVisibility(R.id.WidgetProgressbar, View.GONE);
                    appWidgetManager.updateAppWidget(appWidgetId, views);
                }
            });
        }
    }

}
