package com.l.group.uniscore;


import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.text.InputType;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.facebook.Profile;
import com.theartofdev.edmodo.cropper.CropImage;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

import static android.app.Activity.RESULT_OK;


/**
 * A simple {@link Fragment} subclass.
 */
public class SettingsFragment extends Fragment {

    private static final int RESULT_LOAD_IMG = 1;
    private static final int MY_PERMISSIONS_REQUEST_WRITE_READ = 2;
    UniScoreSDK sdk = new UniScoreSDK(getContext());

    public SettingsFragment() {
        // Required empty public constructor
    }

    public void ChangeProfilePic () {
        if(ContextCompat.checkSelfPermission(getActivity(),Manifest.permission.READ_CALENDAR) != PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(getActivity(),Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(getActivity(),Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[] {Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA}
                    ,MY_PERMISSIONS_REQUEST_WRITE_READ);
        } else {
            CropImage.activity()
                    .setMaxCropResultSize(2048,2048)
                    .setMinCropResultSize(512,512)
                    .start(getContext(), SettingsFragment.this);
        }
    }

    public void DeleteAccount () {


        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle(getString(R.string.DeleteAccountConfirm));


        builder.setPositiveButton(getString(R.string.Confirm), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if(!LoggedInUser.getInstance().isDev) {
                    sdk.DeleteUser(new ResponseListener() {
                        @Override
                        public void OnError(VolleyError error) {
                            Toast toast = Toast.makeText(getContext(), R.string.Failure, Toast.LENGTH_SHORT);
                            toast.setGravity(Gravity.TOP, 0,40);
                            toast.show();
                        }

                        @Override
                        public void OnResponse(Object response) {
                            Toast toast = Toast.makeText(getContext(), R.string.Success, Toast.LENGTH_SHORT);
                            toast.setGravity(Gravity.TOP, 0,40);
                            toast.show();
                            Intent intent = new Intent(getContext(), Login.class);
                            startActivity(intent);
                        }
                    });
                } else {
                    sdk.DeleteGame(new ResponseListener() {
                        @Override
                        public void OnError(VolleyError error) {
                            Toast toast = Toast.makeText(getContext(), R.string.Failure, Toast.LENGTH_SHORT);
                            toast.setGravity(Gravity.TOP, 0,40);
                            toast.show();
                        }

                        @Override
                        public void OnResponse(Object response) {
                            Toast toast = Toast.makeText(getContext(), R.string.Success, Toast.LENGTH_SHORT);
                            toast.setGravity(Gravity.TOP, 0,40);
                            toast.show();
                            Intent intent = new Intent(getContext(), Login.class);
                            startActivity(intent);
                        }
                    });
                }
            }
        });
        builder.setNegativeButton(getString(R.string.No), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        builder.show();


    }

    public void ChangeUsername () {

        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle(getString(R.string.change_username));

        final EditText input = new EditText(getContext());
        input.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_NORMAL);
        builder.setView(input);

        builder.setPositiveButton(getString(R.string.Ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                sdk.ChangeUserUsername(input.getText().toString(), new ResponseListener() {
                    @Override
                    public void OnError(VolleyError error) {
                        Toast toast = Toast.makeText(getContext(), R.string.Failure, Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.TOP, 0,40);
                        toast.show();
                    }

                    @SuppressLint("ShowToast")
                    @Override
                    public void OnResponse(Object response) {
                        Toast toast = Toast.makeText(getContext(), R.string.Success, Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.TOP, 0,40);
                        toast.show();
                    }
                });
            }
        });
        builder.setNegativeButton(getString(R.string.Cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        builder.show();
    }

    public void ChangeEmail () {

        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle(getString(R.string.change_email));

        final EditText input = new EditText(getContext());
        input.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
        builder.setView(input);

        builder.setPositiveButton(getString(R.string.Ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                sdk.ChangeUserEmail(input.getText().toString(), new ResponseListener() {
                    @Override
                    public void OnError(VolleyError error) {
                        Toast toast = Toast.makeText(getContext(), R.string.Failure, Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.TOP, 0,40);
                        toast.show();
                    }

                    @SuppressLint("ShowToast")
                    @Override
                    public void OnResponse(Object response) {
                        Toast toast = Toast.makeText(getContext(), R.string.Success, Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.TOP, 0,40);
                        toast.show();
                    }
                });
            }
        });
        builder.setNegativeButton(getString(R.string.Cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        builder.show();
    }

    public void ChangePassword () {

        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle(getString(R.string.change_password));

        final EditText input = new EditText(getContext());
        input.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
        builder.setView(input);

        builder.setPositiveButton(R.string.Ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                sdk.ChangeUserPassword(input.getText().toString(), new ResponseListener() {
                    @Override
                    public void OnError(VolleyError error) {
                        Toast toast = Toast.makeText(getContext(), R.string.Failure, Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.TOP, 0,40);
                        toast.show();
                    }

                    @SuppressLint("ShowToast")
                    @Override
                    public void OnResponse(Object response) {
                        Toast toast = Toast.makeText(getContext(), R.string.Success, Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.TOP, 0,40);
                        toast.show();
                    }
                });
            }
        });
        builder.setNegativeButton(R.string.Cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        builder.show();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.settings_fragment, container, false);
        Button ChangeUserNameBtn = view.findViewById(R.id.Change_username);
        Button ChangePasswordBtn = view.findViewById(R.id.Change_password);
        Button ChangeEmailBtn = view.findViewById(R.id.Change_email);
        Button ChangePicBtn = view.findViewById(R.id.Change_profilePic);
        Button DeleteBtn = view.findViewById(R.id.Delete_account);

        ChangePicBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) { ChangeProfilePic(); }
        });

        DeleteBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DeleteAccount();
            }
        });

        if(!LoggedInUser.getInstance().isDev) {
            ChangeUserNameBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) { ChangeUsername(); }
            });
        } else  {
            ChangeUserNameBtn.setVisibility(View.GONE);
        }

        if(!LoggedInUser.getInstance().isDev && LoggedInUser.getInstance().fbToken == null) {
            ChangePasswordBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) { ChangePassword(); }
            });
        } else {
            ChangePasswordBtn.setVisibility(View.GONE);
        }

        if(!LoggedInUser.getInstance().isDev) {
            ChangeEmailBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) { ChangeEmail();
                }
            });
        } else {
            ChangeEmailBtn.setVisibility(View.GONE);
        }

        return view;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_WRITE_READ: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    CropImage.activity()
                            .setMaxCropResultSize(2048,2048)
                            .setMinCropResultSize(512,512)
                            .start(getContext(), SettingsFragment.this);
                } return;
            }
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ReqQ.getInstance(getContext()).getRequestQueue().cancelAll("Settings");
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                try {
                    final Uri imageUri = result.getUri();
                    final InputStream imageStream = getContext().getContentResolver().openInputStream(imageUri);
                    final Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);
                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    selectedImage.compress(Bitmap.CompressFormat.JPEG, 60, baos);
                    byte[] byteArrayImage = baos.toByteArray();
                    String encodedImage = Base64.encodeToString(byteArrayImage, Base64.DEFAULT);
                    sdk.PostProfilePicture(encodedImage, new ResponseListener() {
                        @Override
                        public void OnError(VolleyError error) {
                            Toast toast = Toast.makeText(getContext(), R.string.Failure, Toast.LENGTH_SHORT);
                            toast.setGravity(Gravity.TOP, 0,40);
                            toast.show();
                        }

                        @Override
                        public void OnResponse(Object response) {
                            Toast toast = Toast.makeText(getContext(), R.string.Success, Toast.LENGTH_SHORT);
                            toast.setGravity(Gravity.TOP, 0,40);
                            toast.show();
                        }
                    });
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                    Toast.makeText(getActivity(), "Something went wrong", Toast.LENGTH_LONG).show();
                }

            }else {
                Toast.makeText(getActivity(), "You haven't picked Image",Toast.LENGTH_LONG).show();
            }
        }

    }
}
