package com.l.group.uniscore;

import com.facebook.AccessToken;

class LoggedInUser {
    private static final LoggedInUser ourInstance = new LoggedInUser();
    public String id, username, password, email, token;
    public AccessToken fbToken;
    public Boolean isDev = false;

    public void Clear() {
        id=null;
        fbToken = null;
        username=null;
        password=null;
        email=null;
        token=null;
        isDev=null;
    }

    static LoggedInUser getInstance() {
        return ourInstance;
    }

    private LoggedInUser() {
    }
}
