package com.l.group.uniscore;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.VolleyError;

import java.util.List;

public class SearchListAdapter extends BaseAdapter {

    List<Game> games;
    List<User> users;
    String type;
    private LayoutInflater layoutInflater;
    UniScoreSDK sdk;
    Context mContext;

    public SearchListAdapter(List<User> users, List<Game> games, Context mContext, String type) {
        this.users = users;
        this.games = games;
        this.type = type;
        this.layoutInflater = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);;
        this.mContext = mContext;
        this.sdk = new UniScoreSDK(mContext);
    }

    @Override
    public int getCount() {
        if(type.equals(mContext.getString(R.string.games))) {
            return games.size();
        } else if(type.equals(mContext.getString(R.string.users))) {
            return users.size();
        } else return 0;
    }

    @Override
    public Object getItem(int position) {
        if(type.equals(mContext.getString(R.string.games))) {
            return games.get(position);
        } else if(type.equals(mContext.getString(R.string.users))) {
            return users.get(position);
        } else return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public void SetFollowButtonAction (final FloatingActionButton actionButton, final int position) {
        sdk.GetFollowingIds(String.valueOf(LoggedInUser.getInstance().id), new ResponseListener() {
            @Override
            public void OnError(VolleyError error) {

            }

            @Override
            public void OnResponse(Object response) {
                List<User> following = (List<User>) response;
                if(following.contains(users.get(position))) {
                    actionButton.setImageResource(R.drawable.iconfinder_minus_216340);
                    actionButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            sdk.DeleteFollower(users.get(position).id, new ResponseListener() {
                                @Override
                                public void OnError(VolleyError error) {

                                }

                                @Override
                                public void OnResponse(Object response) {
                                    SetFollowButtonAction(actionButton, position);
                                }
                            });
                        }
                    });
                } else {
                    actionButton.setImageResource(R.drawable.plus);
                    actionButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            sdk.PostFollower(users.get(position), new ResponseListener() {
                                @Override
                                public void OnError(VolleyError error) {

                                }

                                @Override
                                public void OnResponse(Object response) {
                                    SetFollowButtonAction(actionButton, position);
                                }
                            });
                        }
                    });
                }
            }
        });
    }

    @Override
    public View getView(final int position, View convertView, final ViewGroup parent) {


        if(convertView == null) {
            convertView =  layoutInflater.inflate(R.layout.table_cell, parent, false);
        }

        ImageView profilePicView = convertView.findViewById(R.id.TableCellImage);
        TextView name= convertView.findViewById(R.id.TableCellInfo1);
        final FloatingActionButton actionButton = convertView.findViewById(R.id.floatingActionButton);

        if(type.equals(mContext.getString(R.string.users))) {
            name.setText(users.get(position).username);
            if(users.get(position).profilePic == null) {
                profilePicView.setImageResource(R.drawable.ic_person_black_24dp);
            } else {
                profilePicView.setImageBitmap(users.get(position).profilePic);
            }
            if(users.get(position).id.equals(LoggedInUser.getInstance().id) || LoggedInUser.getInstance().isDev) {
                actionButton.hide();
            } else {
                SetFollowButtonAction(actionButton, position);
                actionButton.show();
            }

        } else if(type.equals(mContext.getString(R.string.games))) {
            name.setText(games.get(position).name);
            if(games.get(position).profilePic == null) {
                profilePicView.setImageResource(R.drawable.game_icon);
            } else {
                profilePicView.setImageBitmap(games.get(position).profilePic);
            }
            actionButton.hide();
        }

        return convertView;
    }
}
