package com.l.group.uniscore;

class FollowerRelation {

    public int id, followerId, followedId;

    public FollowerRelation (int id, int followerId, int followedId) {
        this.id = id;
        this.followerId = followerId;
        this.followedId = followedId;
    }
}
