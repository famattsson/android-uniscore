package com.l.group.uniscore;


import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;

import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class UserFragment extends Fragment {

    UniScoreSDK sdk;
    public User user;
    public GridView list;
    public TabLayout tabLayout;
    public View view;
    private SwipeRefreshLayout refreshLayout;
    private List<User> following;
    private List<User> followers;
    private List<User> users;

    public UserFragment() {
    }

    void Populate (final List<User> users, String type) {

        view.findViewById(R.id.progressBar).setVisibility(View.VISIBLE);
        view.findViewById(R.id.progressBar2).setVisibility(View.VISIBLE);

        list.setAdapter(new UserTableAdapter(users, type,getContext()));
        ((TextView)view.findViewById(R.id.TopBarInfo)).setText(user.username);

        if(type.equals(getString(R.string.games))) {
            list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Intent intent = new Intent(getContext(), EmptyActivity.class);
                    intent.putExtra("TYPE", "Games");
                    intent.putExtra("id", String.valueOf(users.get(0).scores.get(position).game.id));
                    startActivity(intent);
                }
            });
        } else if (type.equals(getString(R.string.followers)) || type.equals(getString(R.string.following))) {
            list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Intent intent = new Intent(getContext(), EmptyActivity.class);
                    intent.putExtra("TYPE", "Users");
                    intent.putExtra("id",users.get(position).id);
                    startActivity(intent);
                }
            });
        }



        view.findViewById(R.id.progressBar).setVisibility(View.GONE);
        view.findViewById(R.id.progressBar2).setVisibility(View.GONE);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_users, container, false);
        sdk = new UniScoreSDK(getContext());
        tabLayout = view.findViewById(R.id.UserTabs);
        list = view.findViewById(R.id.UserList);
        refreshLayout = view.findViewById(R.id.swipeRefresh);
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                String type = tab.getText().toString();
                if(type.equals(getString(R.string.games))) {
                    Populate(users, type);
                } else if (type.equals(getString(R.string.followers))) {
                    Populate(followers, type);
                } else if (type.equals(getString(R.string.following))) {
                    Populate(following, type);
                }

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
        refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                GetUserData();
            }
        });
        GetUserData();
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ReqQ.getInstance(getContext()).getRequestQueue().cancelAll("User");
    }

    public void GetUserData () {
        sdk.GetUser(getArguments().getString("userId"), new ResponseListener() {
            @Override
            public void OnError(VolleyError error) {
                Toast toast = Toast.makeText(getContext(), R.string.Failure, Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.TOP, 0,40);
                toast.show();
            }

            @Override
            public void OnResponse(Object response) {
                if(getContext() != null) {
                    users = (List<User>) response;
                    user = users.get(0);
                    if(user.scores.size() == 0) {
                        TextView textView = new TextView(getContext());
                        textView.setText(getString(R.string.UserScoreListError));
                        ((LinearLayout) view.findViewById(R.id.UserLayout)).addView(textView, 2);
                    }
                    sdk.GetFollowers(String.valueOf(user.id), new ResponseListener() {
                        @Override
                        public void OnError(VolleyError error) {
                            Toast toast = Toast.makeText(getContext(), R.string.Failure, Toast.LENGTH_SHORT);
                            toast.setGravity(Gravity.TOP, 0,40);
                            toast.show();
                        }

                        @Override
                        public void OnResponse(Object response) {
                            if(getContext() != null) {
                                followers = (List<User>) response;
                                sdk.GetFollowing(String.valueOf(user.id), new ResponseListener() {
                                    @Override
                                    public void OnError(VolleyError error) {

                                    }

                                    @Override
                                    public void OnResponse(Object response) {
                                        if(getContext()!= null) {
                                            final ImageView profilePicView = view.findViewById(R.id.TopBarImage);
                                            if(users.get(0).profilePic != null) {
                                                profilePicView.setImageBitmap(users.get(0).profilePic);
                                            } else {
                                                profilePicView.setImageResource(R.drawable.ic_person_black_24dp);
                                            }
                                            view.requestLayout();
                                            following = (List<User>) response;

                                            String type = tabLayout.getTabAt(tabLayout.getSelectedTabPosition()).getText().toString();

                                            String test = getString(R.string.games);

                                            if(type.equals(getString(R.string.games))) {
                                                Populate(users, type);
                                            } else if (type.equals(getString(R.string.followers))) {
                                                Populate(followers, type);
                                            } else if (type.equals(getString(R.string.following))) {
                                                Populate(following, type);
                                            }
                                            refreshLayout.setRefreshing(false);
                                        }

                                    }
                                });
                            }

                        }
                    });
                }

            }

        });
    }

}
