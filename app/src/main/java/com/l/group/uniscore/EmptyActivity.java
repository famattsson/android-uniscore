package com.l.group.uniscore;

import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class EmptyActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_empty);
        String type = getIntent().getStringExtra("TYPE");
        if(type.equals("Users")) {
            UserFragment userFragment = new UserFragment();
            Bundle bundle = new Bundle();
            bundle.putString("userId", getIntent().getStringExtra("id"));
            userFragment.setArguments(bundle);
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.Fragment_container, userFragment);
            fragmentTransaction.commit();
        } else if(type.equals("Games")) {
            GameFragment gameFragment = new GameFragment();
            Bundle bundle = new Bundle();
            Bundle extras = getIntent().getExtras();
            String id = extras.getString("id");
            bundle.putString("gameId", getIntent().getStringExtra("id"));
            gameFragment.setArguments(bundle);
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.Fragment_container, gameFragment);
            fragmentTransaction.commit();
        }
    }
}
