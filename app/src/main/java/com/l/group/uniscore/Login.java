package com.l.group.uniscore;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.InputType;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.Profile;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;

public class Login extends AppCompatActivity {

    private static final String TAG = "tag";
    EditText usernameInput;
    EditText passwordInput;
    EditText emailInput;
    UniScoreSDK SDK = new UniScoreSDK(this);
    boolean isDev = false;
    CallbackManager callbackManager;
    Profile fbProfile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        callbackManager = CallbackManager.Factory.create();
        printHashKey(this);
        if(AccessToken.getCurrentAccessToken() != null) {
            SDK.FacebookLogin(AccessToken.getCurrentAccessToken(), new ResponseListener() {
                @Override
                public void OnError(VolleyError error) {
                    Toast toast = Toast.makeText(Login.this, R.string.Failure, Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.TOP, 0,40);
                    toast.show();
                }

                @Override
                public void OnResponse(Object response) {
                    Intent i = new Intent(Login.this, HomeActivity.class);
                    startActivity(i);
                    overridePendingTransition(R.anim.fade_in,R.anim.fade_out);
                }
            });
        }
        LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(final LoginResult loginResult) {
                fbProfile = Profile.getCurrentProfile();
                SDK.GetFacebookUser(fbProfile.getId(), new ResponseListener() {
                    @Override
                    public void OnError(VolleyError error) {
                        Toast toast = Toast.makeText(Login.this, R.string.Failure, Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.TOP, 0,40);
                        toast.show();
                    }

                    @Override
                    public void OnResponse(Object response) {
                        if(((List<User>) response).size() != 0) {
                            SDK.FacebookLogin(loginResult.getAccessToken(), new ResponseListener() {
                                @Override
                                public void OnError(VolleyError error) {
                                    Toast toast = Toast.makeText(Login.this, R.string.Failure, Toast.LENGTH_SHORT);
                                    toast.setGravity(Gravity.TOP, 0,40);
                                    toast.show();
                                }

                                @Override
                                public void OnResponse(Object response) {
                                    Intent i = new Intent(Login.this, HomeActivity.class);
                                    startActivity(i);
                                    overridePendingTransition(R.anim.fade_in,R.anim.fade_out);
                                }
                            });
                        } else {
                            AlertDialog.Builder builder = new AlertDialog.Builder(Login.this);
                            builder.setTitle(R.string.TPASignUpUsernamePrompt);

                            final EditText input = new EditText(Login.this);
                            input.setInputType(InputType.TYPE_CLASS_TEXT);
                            builder.setView(input);

                            builder.setPositiveButton(R.string.Ok, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    SDK.FacebookSignUp(input.getText().toString(), loginResult.getAccessToken(), fbProfile.getId(), null, null, new ResponseListener() {
                                        @Override
                                        public void OnError(VolleyError error) {
                                            Toast toast = Toast.makeText(Login.this, R.string.Failure, Toast.LENGTH_SHORT);
                                            toast.setGravity(Gravity.TOP, 0,40);
                                            toast.show();
                                        }

                                        @Override
                                        public void OnResponse(Object response) {
                                            Intent i = new Intent(Login.this, HomeActivity.class);
                                            startActivity(i);
                                            overridePendingTransition(R.anim.fade_in,R.anim.fade_out);
                                        }
                                    });
                                }
                            });
                            builder.setNegativeButton(R.string.Cancel, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.cancel();
                                }
                            });
                            builder.show();
                        }
                    }
                });
            }

            @Override
            public void onCancel() {

            }

            @Override
            public void onError(FacebookException error) {

            }
        });

        setContentView(R.layout.activity_login);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
    }

    public void printHashKey(Context pContext) {
        try {
            PackageInfo info = getPackageManager().getPackageInfo(getPackageName(), PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                String hashKey = new String(Base64.encode(md.digest(), 0));
                Log.i(TAG, "printHashKey() Hash Key: " + hashKey);
            }
        } catch (NoSuchAlgorithmException e) {
            Log.e(TAG, "printHashKey()", e);
        } catch (Exception e) {
            Log.e(TAG, "printHashKey()", e);
        }
    }

    public void LoginOnClick (View view) {
        usernameInput = findViewById(R.id.usernameInput);
        passwordInput = findViewById(R.id.passwordInput);
        if(!isDev) {
            SDK.LogIn(usernameInput.getText().toString(), passwordInput.getText().toString(), new ResponseListener() {
                @Override
                public void OnError(VolleyError error) {
                    Toast toast = Toast.makeText(Login.this, R.string.Failure, Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.TOP, 0,40);
                    toast.show();
                }

                @Override
                public void OnResponse(Object response) {
                    if (LoggedInUser.getInstance().token!=null) {
                        Intent i = new Intent(Login.this, HomeActivity.class);
                        startActivity(i);
                        overridePendingTransition(R.anim.fade_in,R.anim.fade_out);
                    }
                }
            });
        } else {
            SDK.GetGameCredentials(usernameInput.getText().toString(), passwordInput.getText().toString(), new ResponseListener() {
                @Override
                public void OnError(VolleyError error) {
                    Toast toast = Toast.makeText(Login.this, R.string.Failure, Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.TOP, 0,40);
                    toast.show();
                }

                @Override
                public void OnResponse(Object response) {
                    if (LoggedInUser.getInstance().token!=null) {
                        Intent i = new Intent(Login.this, HomeActivity.class);
                        startActivity(i);
                        overridePendingTransition(R.anim.fade_in,R.anim.fade_out);
                    }
                }
            });
        }

    }

    public void onRadioButtonClicked(View view) {
        boolean checked = ((RadioButton) view).isChecked();

        switch (view.getId()) {
            case R.id.radioButtonDev:
                if(checked) {
                    isDev = true;
                    findViewById(R.id.SignUp).setVisibility(View.GONE);
                }
                break;
            case R.id.radioButtonPlayer:
                if(checked) {
                    isDev = false;
                    findViewById(R.id.SignUp).setVisibility(View.VISIBLE);
                }
                break;
            default:
                break;
        }
    }

    public void SignUpOnClick (View view) {
        usernameInput = findViewById(R.id.usernameInput);
        passwordInput = findViewById(R.id.passwordInput);
        emailInput = findViewById(R.id.emailInput);
        SDK.SignUp(usernameInput.getText().toString(), null, passwordInput.getText().toString(), emailInput.getText().toString(), new ResponseListener() {
            @Override
            public void OnError(VolleyError error) {
                Toast toast = Toast.makeText(Login.this, R.string.Failure, Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.TOP, 0,40);
                toast.show();
            }

            @Override
            public void OnResponse(Object response) {
                if (LoggedInUser.getInstance().token!=null) {
                    Intent i = new Intent(Login.this, HomeActivity.class);
                    startActivity(i);
                    overridePendingTransition(R.anim.fade_in,R.anim.fade_out);
                }
            }
        });
    }

}
