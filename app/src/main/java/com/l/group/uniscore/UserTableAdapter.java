package com.l.group.uniscore;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.Uri;
import android.support.design.widget.FloatingActionButton;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.VolleyError;

import java.util.List;

public class UserTableAdapter extends BaseAdapter {

    List<User> users;
    private LayoutInflater layoutInflater;
    String type;
    Context mContext;
    UniScoreSDK sdk;

    public UserTableAdapter(List<User> users, String type, Context mContext) {
        this.type = type;
        this.users = users;
        this.layoutInflater = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);;
        this.mContext = mContext;
        sdk = new UniScoreSDK(mContext);
    }

    @Override
    public int getCount() {
        if(type.equals(mContext.getString(R.string.games))) {
            return users.get(0).scores.size();
        } else if (type.equals(mContext.getString(R.string.followers)) || type.equals(mContext.getString(R.string.following))) {
            return  users.size();
        } else return 0;
    }

    @Override
    public Object getItem(int position) {
        if(type.equals(mContext.getString(R.string.games))) {
            return users.get(0).scores.get(position);
        } else if (type.equals(mContext.getString(R.string.followers)) || type.equals(mContext.getString(R.string.following))) {
            return users.get(position);
        } else return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public void SetFollowButtonAction (final FloatingActionButton actionButton, final int position) {
        sdk.GetFollowingIds(String.valueOf(LoggedInUser.getInstance().id), new ResponseListener() {
            @Override
            public void OnError(VolleyError error) {

            }

            @Override
            public void OnResponse(Object response) {
                List<User> following = (List<User>) response;
                if(following.contains(users.get(position))) {
                    actionButton.setImageResource(R.drawable.iconfinder_minus_216340);
                    actionButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            sdk.DeleteFollower(users.get(position).id, new ResponseListener() {
                                @Override
                                public void OnError(VolleyError error) {

                                }

                                @Override
                                public void OnResponse(Object response) {
                                    SetFollowButtonAction(actionButton, position);
                                }
                            });
                        }
                    });
                } else {
                    actionButton.setImageResource(R.drawable.plus);
                    actionButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            sdk.PostFollower(users.get(position), new ResponseListener() {
                                @Override
                                public void OnError(VolleyError error) {

                                }

                                @Override
                                public void OnResponse(Object response) {
                                    SetFollowButtonAction(actionButton, position);
                                }
                            });
                        }
                    });
                }
            }
        });
    }

    @Override
    public View getView(final int position, View convertView, final ViewGroup parent) {

        if(convertView == null) {
            convertView =  layoutInflater.inflate(R.layout.table_cell, parent, false);
        }

        TextView name = convertView.findViewById(R.id.TableCellInfo1);
        TextView scoreText = convertView.findViewById(R.id.TableCellInfo2);
        FloatingActionButton actionButton = convertView.findViewById(R.id.floatingActionButton);
        ImageView imageView = convertView.findViewById(R.id.TableCellImage);

        if(type.equals(mContext.getString(R.string.games))) {
            name.setText(users.get(0).scores.get(position).game.name);
            scoreText.setText(String.valueOf(users.get(0).scores.get(position).score));
            if(users.get(0).scores.get(position).game.profilePic == null) {
                imageView.setImageResource(R.drawable.game_icon);
            } else {
                imageView.setImageBitmap(users.get(0).scores.get(position).game.profilePic);
            }
            if(LoggedInUser.getInstance().id != null) {
                if(LoggedInUser.getInstance().id.equals(String.valueOf(users.get(0).scores.get(position).gameId))
                        || LoggedInUser.getInstance().id.equals(String.valueOf(users.get(0).scores.get(position).userId))) {
                    actionButton.show();
                    actionButton.setImageResource(R.drawable.iconfinder_close_icon_1398919);
                    actionButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                            builder.setTitle("Are you sure you want to delete this score?");
                            builder.setPositiveButton(R.string.Confirm, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    sdk.DeleteScore(String.valueOf(users.get(0).scores.get(position).id),new ResponseListener() {
                                        @Override
                                        public void OnError(VolleyError error) {

                                        }

                                        @Override
                                        public void OnResponse(Object response) {
                                            parent.invalidate();
                                        }
                                    });
                                }
                            });
                            builder.setNegativeButton(R.string.No, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.cancel();
                                }
                            });
                            builder.show();
                        }
                    });
                } else {
                    actionButton.hide();
                }
            } else {
                actionButton.hide();
            }
        } else if (type.equals(mContext.getString(R.string.followers)) || type.equals(mContext.getString(R.string.following))) {
            name.setText(users.get(position).username);
            if(users.get(position).profilePic == null) {
                imageView.setImageResource(R.drawable.ic_person_black_24dp);
            } else {
                imageView.setImageBitmap(users.get(position).profilePic);
            }
            if(users.get(position).id.equals(LoggedInUser.getInstance().id) || LoggedInUser.getInstance().isDev || LoggedInUser.getInstance().id == null) {
                actionButton.hide();
            } else {
                actionButton.show();
                SetFollowButtonAction(actionButton, position);
            }
        }
        return convertView;
    }
}
