package com.l.group.uniscore;

import android.app.DownloadManager;
import android.content.Context;
import android.graphics.Bitmap;
import android.support.v4.graphics.BitmapCompat;
import android.util.Base64;
import android.widget.ImageView;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.ImageRequest;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.JsonRequest;
import com.android.volley.toolbox.StringRequest;
import com.auth0.android.jwt.DecodeException;
import com.auth0.android.jwt.JWT;
import com.facebook.AccessToken;
import com.facebook.Profile;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.security.acl.LastOwnerException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class UniScoreSDK {

    //User functions

    private Context mContext;

    public UniScoreSDK (Context context) {
        mContext = context;
    }

    public void LogIn (final String username, final String password, final ResponseListener listener) {

        String url = "http://uniscoreapp-env.krgm9ru437.eu-west-1.elasticbeanstalk.com/User-login-tokens";


        StringRequest LoginReq = new StringRequest(
                Request.Method.POST,
                url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        JWT token = new JWT(response);
                        LoggedInUser.getInstance().id = token.getClaim("id").asString();
                        LoggedInUser.getInstance().password = password;
                        LoggedInUser.getInstance().username = username;
                        LoggedInUser.getInstance().token = token.toString();
                        LoggedInUser.getInstance().isDev = token.getClaim("isGame").asBoolean();
                        String getUrl = String.format("http://uniscoreapp-env.krgm9ru437.eu-west-1.elasticbeanstalk.com/Users?id=%s",LoggedInUser.getInstance().id);
                        final StringRequest getRequest = new StringRequest(Request.Method.GET, getUrl, new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                try {
                                    String fixedRes = response.substring(1,response.length()-1);
                                    JSONObject jsonObject = new JSONObject(fixedRes);
                                    LoggedInUser.getInstance().email = jsonObject.getString("Email");
                                    listener.OnResponse(response);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {

                            }
                        });
                        ReqQ.getInstance(mContext).addToRequestQueue(getRequest);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        listener.OnError(error);
                    }
                }){

            @Override
            public String getBodyContentType() {
                return "application/x-www-form-urlencoded; charset=UTF-8";
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("username",username);
                params.put("password",password);
                params.put("grant_type","password");
                return params;
            }

        };
        ReqQ.getInstance(mContext).addToRequestQueue(LoginReq);
    }

    public void  FacebookLogin (final AccessToken fbToken, final ResponseListener listener) {
        String url = "http://uniscoreapp-env.krgm9ru437.eu-west-1.elasticbeanstalk.com/User-login-tokens";


        StringRequest LoginReq = new StringRequest(
                Request.Method.POST,
                url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        final JWT token = new JWT(response);
                        LoggedInUser.getInstance().id = token.getClaim("id").asString();
                        LoggedInUser.getInstance().token = token.toString();
                        LoggedInUser.getInstance().isDev = token.getClaim("isGame").asBoolean();
                        String getUrl = String.format("http://uniscoreapp-env.krgm9ru437.eu-west-1.elasticbeanstalk.com/Users?id=%s",LoggedInUser.getInstance().id);
                        final StringRequest getRequest = new StringRequest(Request.Method.GET, getUrl, new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                try {
                                    String fixedRes = response.substring(1,response.length()-1);
                                    JSONObject jsonObject = new JSONObject(fixedRes);
                                    LoggedInUser.getInstance().username = jsonObject.getString("Username");
                                    LoggedInUser.getInstance().email = jsonObject.getString("Email");
                                    LoggedInUser.getInstance().fbToken = fbToken;
                                    listener.OnResponse(response);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {

                            }
                        });
                        ReqQ.getInstance(mContext).addToRequestQueue(getRequest);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        String msg = new String(error.networkResponse.data);
                        listener.OnError(error);
                    }
                }){

            @Override
            public String getBodyContentType() {
                return "application/x-www-form-urlencoded; charset=UTF-8";
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("accessToken", fbToken.getToken());
                params.put("grant_type","facebookToken");
                return params;
            }

        };
        ReqQ.getInstance(mContext).addToRequestQueue(LoginReq);
    }

    public void SignUp (final String username, final String facebookId, final String password, final String email, final ResponseListener listener) {
        String url = "http://uniscoreapp-env.krgm9ru437.eu-west-1.elasticbeanstalk.com/Users";
        Map<String, String> params = new HashMap<String, String>();
        params.put("username", username);
        params.put("password", password);
        params.put("facebookId", facebookId);
        params.put("email", email);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, new JSONObject(params), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                LogIn(username,password,listener);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                String msg = new String(error.networkResponse.data);
                listener.OnError(error);
            }
        });
        ReqQ.getInstance(mContext).addToRequestQueue(jsonObjectRequest);
    }

    public void FacebookSignUp (final String username, final AccessToken accessToken, final String facebookId, final String password, final String email, final ResponseListener listener) {
        String url = "http://uniscoreapp-env.krgm9ru437.eu-west-1.elasticbeanstalk.com/Users";
        Map<String, String> params = new HashMap<String, String>();
        params.put("username", username);
        params.put("password", password);
        params.put("facebookId", facebookId);
        params.put("email", email);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, new JSONObject(params), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                FacebookLogin(accessToken,listener);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                String msg = new String(error.networkResponse.data);
                listener.OnError(error);
            }
        });
        ReqQ.getInstance(mContext).addToRequestQueue(jsonObjectRequest);
    }

    public void ChangeUserUsername(final String username, final ResponseListener listener) {
        String url = "http://uniscoreapp-env.krgm9ru437.eu-west-1.elasticbeanstalk.com/Users";
        Map<String,String> params = new HashMap<String, String>();
        params.put("token",LoggedInUser.getInstance().token);
        params.put("username",username);
        JsonObjectRequest patchRequest = new JsonObjectRequest(Request.Method.PATCH, url, new JSONObject(params), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                if(LoggedInUser.getInstance().fbToken != null) {
                    FacebookLogin(LoggedInUser.getInstance().fbToken, listener);
                } else {
                    LogIn(username, LoggedInUser.getInstance().password, listener);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                listener.OnError(error);
            }
        });

        ReqQ.getInstance(mContext).addToRequestQueue(patchRequest);
    }

    public void ChangeUserPassword(final String password, final ResponseListener listener) {
        String url = "http://uniscoreapp-env.krgm9ru437.eu-west-1.elasticbeanstalk.com/Users";
        Map<String,String> params = new HashMap<String, String>();
        params.put("token",LoggedInUser.getInstance().token);
        params.put("password",password);
        JsonObjectRequest patchRequest = new JsonObjectRequest(Request.Method.PATCH, url, new JSONObject(params), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                if(LoggedInUser.getInstance().fbToken != null) {
                    FacebookLogin(LoggedInUser.getInstance().fbToken, listener);
                } else {
                    LogIn(LoggedInUser.getInstance().username, password, listener);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                listener.OnError(error);
            }
        });

        ReqQ.getInstance(mContext).addToRequestQueue(patchRequest);
    }

    public void ChangeUserEmail(final String email, final ResponseListener listener) {
        String url = "http://uniscoreapp-env.krgm9ru437.eu-west-1.elasticbeanstalk.com/Users";
        Map<String,String> params = new HashMap<String, String>();
        params.put("token",LoggedInUser.getInstance().token);
        params.put("email",email);
        JsonObjectRequest patchRequest = new JsonObjectRequest(Request.Method.PATCH, url, new JSONObject(params), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                if(LoggedInUser.getInstance().fbToken != null) {
                    FacebookLogin(LoggedInUser.getInstance().fbToken, listener);
                } else {
                    LogIn(LoggedInUser.getInstance().username, LoggedInUser.getInstance().password, listener);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                listener.OnError(error);
            }
        });

        ReqQ.getInstance(mContext).addToRequestQueue(patchRequest);
    }

    public void DeleteUser(final ResponseListener listener) {
        String url = String.format("http://uniscoreapp-env.krgm9ru437.eu-west-1.elasticbeanstalk.com/Users?token=%s", LoggedInUser.getInstance().token);

        StringRequest deleteReq = new StringRequest(Request.Method.DELETE, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                LoggedInUser.getInstance().Clear();
                listener.OnResponse(null);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                String body = new String(error.networkResponse.data);
                listener.OnError(error);
            }
        });
        ReqQ.getInstance(mContext).addToRequestQueue(deleteReq);
    }

    public void GetFacebookUser(String fbId, final ResponseListener listener) {
        String url = String.format("http://uniscoreapp-env.krgm9ru437.eu-west-1.elasticbeanstalk.com/Users?facebookId=%s", fbId);

        StringRequest getRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    LoadedResources.getInstance().users.clear();
                    final List<User> users = new ArrayList<>();
                    final JSONArray responseJSONArray = new JSONArray(response);
                    if(responseJSONArray.length()==0) {
                        listener.OnResponse(users);
                    }
                    for (int i = 0; i < responseJSONArray.length(); i++) {
                        final String id = responseJSONArray.getJSONObject(i).getString("Id");
                        final String username = responseJSONArray.getJSONObject(i).getString("Username");
                        final String email = responseJSONArray.getJSONObject(i).getString("Email");
                        GetScoresByUserId(String.valueOf(id), new ResponseListener() {
                            @Override
                            public void OnError(VolleyError error) {

                            }

                            @Override
                            public void OnResponse(Object response) {

                                final List<Score> scores = (List<Score>) response;
                                GetProfilePicture('U' + String.valueOf(id), new ResponseListener() {
                                    @Override
                                    public void OnError(VolleyError error) {
                                        User user = new User(id,username,email, scores, null);
                                        users.add(user);
                                        if(responseJSONArray.length() == users.size()) {
                                            listener.OnResponse(users);
                                        }
                                    }

                                    @Override
                                    public void OnResponse(Object response) {
                                        User user = new User(id,username,email, scores, (Bitmap) response);
                                        users.add(user);
                                        if(responseJSONArray.length() == users.size()) {
                                            listener.OnResponse(users);
                                        }
                                    }
                                });
                            }
                        });
                    }
                } catch (JSONException jExcept) {
                    jExcept.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                String body = new String(error.networkResponse.data);
                listener.OnError(error);
            }
        });
        ReqQ.getInstance(mContext).addToRequestQueue(getRequest);
    }

    public void GetUsers(String username, final ResponseListener listener) {
        String url = String.format("http://uniscoreapp-env.krgm9ru437.eu-west-1.elasticbeanstalk.com/Users?username=%s", username);

        StringRequest getRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    LoadedResources.getInstance().users.clear();
                    final List<User> users = new ArrayList<>();
                    final JSONArray responseJSONArray = new JSONArray(response);
                    if(responseJSONArray.length()==0) {
                        listener.OnResponse(users);
                    }
                    for (int i = 0; i < responseJSONArray.length(); i++) {
                        final String id = responseJSONArray.getJSONObject(i).getString("Id");
                        final String username = responseJSONArray.getJSONObject(i).getString("Username");
                        final String email = responseJSONArray.getJSONObject(i).getString("Email");
                        GetScoresByUserId(String.valueOf(id), new ResponseListener() {
                            @Override
                            public void OnError(VolleyError error) {

                            }

                            @Override
                            public void OnResponse(Object response) {

                                final List<Score> scores = (List<Score>) response;
                                GetProfilePicture('U' + String.valueOf(id), new ResponseListener() {
                                    @Override
                                    public void OnError(VolleyError error) {
                                        User user = new User(id,username,email, scores, null);
                                        users.add(user);
                                        if(responseJSONArray.length() == users.size()) {
                                            listener.OnResponse(users);
                                        }
                                    }

                                    @Override
                                    public void OnResponse(Object response) {
                                        User user = new User(id,username,email, scores, (Bitmap) response);
                                        users.add(user);
                                        if(responseJSONArray.length() == users.size()) {
                                            listener.OnResponse(users);
                                        }
                                    }
                                });
                            }
                        });
                    }
                } catch (JSONException jExcept) {
                    jExcept.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                String body = new String(error.networkResponse.data);
                listener.OnError(error);
            }
        });
        ReqQ.getInstance(mContext).addToRequestQueue(getRequest);
    }

    public void GetUser(String id, final ResponseListener listener) {
        String url = String.format("http://uniscoreapp-env.krgm9ru437.eu-west-1.elasticbeanstalk.com/Users?id=%s", id);

        StringRequest getRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    LoadedResources.getInstance().users.clear();
                    final List<User> users = new ArrayList<>();
                    final JSONArray responseJSONArray = new JSONArray(response);
                    if(responseJSONArray.length()==0) {
                        listener.OnResponse(users);
                    }
                    for (int i = 0; i < responseJSONArray.length(); i++) {
                        final String id = responseJSONArray.getJSONObject(i).getString("Id");
                        final String username = responseJSONArray.getJSONObject(i).getString("Username");
                        final String email = responseJSONArray.getJSONObject(i).getString("Email");
                        GetScoresByUserId(String.valueOf(id), new ResponseListener() {
                            @Override
                            public void OnError(VolleyError error) {

                            }

                            @Override
                            public void OnResponse(Object response) {

                                final List<Score> scores = (List<Score>) response;
                                GetProfilePicture('U' + String.valueOf(id), new ResponseListener() {
                                    @Override
                                    public void OnError(VolleyError error) {
                                        User user = new User(id,username,email, scores, null);
                                        users.add(user);
                                        if(responseJSONArray.length() == users.size()) {
                                            listener.OnResponse(users);
                                        }
                                    }

                                    @Override
                                    public void OnResponse(Object response) {
                                        User user = new User(id,username,email, scores, (Bitmap) response);
                                        users.add(user);
                                        if(responseJSONArray.length() == users.size()) {
                                            listener.OnResponse(users);
                                        }
                                    }
                                });
                            }
                        });
                    }
                } catch (JSONException jExcept) {
                    jExcept.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                String body = new String(error.networkResponse.data);
                listener.OnError(error);
            }
        });
        ReqQ.getInstance(mContext).addToRequestQueue(getRequest);
    }

    //Game functions

    public void GetGameCredentials (final String username, final String password, final ResponseListener listener) {

        String url = "http://uniscoreapp-env.krgm9ru437.eu-west-1.elasticbeanstalk.com/Game-login-tokens";

        StringRequest LoginReq = new StringRequest(
                Request.Method.POST,
                url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        JWT token = new JWT(response);
                        LoggedInUser.getInstance().id = token.getClaim("id").asString();
                        LoggedInUser.getInstance().password = password;
                        LoggedInUser.getInstance().username = username;
                        LoggedInUser.getInstance().token = token.toString();
                        LoggedInUser.getInstance().isDev = token.getClaim("isGame").asBoolean();
                        String getUrl = String.format("http://uniscoreapp-env.krgm9ru437.eu-west-1.elasticbeanstalk.com/Games?id=%s",LoggedInUser.getInstance().id);
                        listener.OnResponse(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        String msg = new String(error.networkResponse.data);
                        listener.OnError(error);
                    }
                }){

            @Override
            public String getBodyContentType() {
                return "application/x-www-form-urlencoded; charset=UTF-8";
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("name",username);
                params.put("password",password);
                params.put("grant_type","password");
                return params;
            }

        };
        ReqQ.getInstance(mContext).addToRequestQueue(LoginReq);
    }

    public void DeleteGame (final ResponseListener listener) {
        String url = String.format("http://uniscoreapp-env.krgm9ru437.eu-west-1.elasticbeanstalk.com/Games?token=%s", LoggedInUser.getInstance().token);

        StringRequest deleteReq = new StringRequest(Request.Method.DELETE, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                LoggedInUser.getInstance().Clear();
                listener.OnResponse(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                String body = new String(error.networkResponse.data);
                listener.OnError(error);
            }
        });
        ReqQ.getInstance(mContext).addToRequestQueue(deleteReq);
    }

    public void GetGames (final String name, final ResponseListener listener) {
        String url = String.format("http://uniscoreapp-env.krgm9ru437.eu-west-1.elasticbeanstalk.com/Games?name=%s", name);

        JsonArrayRequest getRequest = new JsonArrayRequest(Request.Method.GET, url, null, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                try {
                    final JSONArray jsonArray = response;
                    if(LoadedResources.getInstance().games != null)
                        LoadedResources.getInstance().games.clear();
                    final List<Game> games = new ArrayList<>();
                    if(response.length()==0) {
                        listener.OnResponse(games);
                    }
                    for (int i = 0; i < response.length(); i++) {
                        final int id = response.getJSONObject(i).getInt("Id");
                        final String name = response.getJSONObject(i).getString("Name");
                        final String developerName = response.getJSONObject(i).getString("DeveloperName");
                        final String releaseDate = response.getJSONObject(i).getString("ReleaseDate");
                        final String publisherName = response.getJSONObject(i).getString("PublisherName");
                        final String genreName = response.getJSONObject(i).getString("GenreName");
                        GetScoresByGameId(String.valueOf(id), new ResponseListener() {
                            @Override
                            public void OnError(VolleyError error) {

                            }

                            @Override
                            public void OnResponse(Object response) {

                                final List<Score> scores = (List<Score>) response;

                                GetProfilePicture('G' + String.valueOf(id), new ResponseListener() {
                                    @Override
                                    public void OnError(VolleyError error) {
                                        Game game = new Game(scores,id, name, releaseDate, developerName, genreName, publisherName, null);
                                        games.add(game);
                                        if(jsonArray.length() == games.size()) {
                                            listener.OnResponse(games);
                                        }
                                    }

                                    @Override
                                    public void OnResponse(Object response) {
                                        Game game = new Game(scores,id, name, releaseDate, developerName, genreName, publisherName, (Bitmap) response);
                                        games.add(game);
                                        if(jsonArray.length() == games.size()) {
                                            listener.OnResponse(games);
                                        }
                                    }
                                });
                                if(games.size() == jsonArray.length()) {
                                    listener.OnResponse(games);
                                }
                            }
                        });
                    }

                } catch (JSONException jExcept) {
                    //Handle error
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                String body = new String(error.networkResponse.data);
                listener.OnError(error);
            }
        });
        ReqQ.getInstance(mContext).addToRequestQueue(getRequest);
    }

    public void  ChangeDevName (final String devName, final ResponseListener listener) {
        String url = "http://uniscoreapp-env.krgm9ru437.eu-west-1.elasticbeanstalk.com/Games";
        Map<String,String> params = new HashMap<String, String>();
        params.put("token",LoggedInUser.getInstance().token);
        params.put("developerName",devName);
        JsonObjectRequest patchRequest = new JsonObjectRequest(Request.Method.PATCH, url, new JSONObject(params), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                GetGameCredentials(LoggedInUser.getInstance().username, devName, listener);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                listener.OnError(error);
            }
        });

        ReqQ.getInstance(mContext).addToRequestQueue(patchRequest);
    }

    public void  ChangeReleaseDate (final String relDate, final ResponseListener listener) {
        String url = "http://uniscoreapp-env.krgm9ru437.eu-west-1.elasticbeanstalk.com/Games";
        Map<String,String> params = new HashMap<String, String>();
        params.put("token",LoggedInUser.getInstance().token);
        params.put("releaseDate",relDate);
        JsonObjectRequest patchRequest = new JsonObjectRequest(Request.Method.PATCH, url, new JSONObject(params), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                GetGameCredentials(LoggedInUser.getInstance().username, relDate, listener);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                listener.OnError(error);
            }
        });

        ReqQ.getInstance(mContext).addToRequestQueue(patchRequest);
    }

    public void  ChangePubName (final String pubName, final ResponseListener listener) {
        String url = "http://uniscoreapp-env.krgm9ru437.eu-west-1.elasticbeanstalk.com/Games";
        Map<String,String> params = new HashMap<String, String>();
        params.put("token",LoggedInUser.getInstance().token);
        params.put("publisherName",pubName);
        JsonObjectRequest patchRequest = new JsonObjectRequest(Request.Method.PATCH, url, new JSONObject(params), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                GetGameCredentials(LoggedInUser.getInstance().username, pubName, listener);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                listener.OnError(error);
            }
        });

        ReqQ.getInstance(mContext).addToRequestQueue(patchRequest);
    }

    public void GetGame (String id, final ResponseListener listener) {
        String url = String.format("http://uniscoreapp-env.krgm9ru437.eu-west-1.elasticbeanstalk.com/Games?id=%s",id);

        JsonArrayRequest getRequest = new JsonArrayRequest(Request.Method.GET, url, null, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                try {
                    final JSONArray jsonArray = response;
                    if(LoadedResources.getInstance().games != null)
                        LoadedResources.getInstance().games.clear();
                    final List<Game> games = new ArrayList<>();
                    if(response.length()==0) {
                        listener.OnResponse(games);
                    }
                    for (int i = 0; i < response.length(); i++) {
                        final int id = response.getJSONObject(i).getInt("Id");
                        final String name = response.getJSONObject(i).getString("Name");
                        final String developerName = response.getJSONObject(i).getString("DeveloperName");
                        final String releaseDate = response.getJSONObject(i).getString("ReleaseDate");
                        final String publisherName = response.getJSONObject(i).getString("PublisherName");
                        final String genreName = response.getJSONObject(i).getString("GenreName");
                        GetScoresByGameId(String.valueOf(id), new ResponseListener() {
                            @Override
                            public void OnError(VolleyError error) {

                            }

                            @Override
                            public void OnResponse(Object response) {

                                final List<Score> scores = (List<Score>) response;

                                GetProfilePicture('G' + String.valueOf(id), new ResponseListener() {
                                    @Override
                                    public void OnError(VolleyError error) {
                                        Game game = new Game(scores,id, name, releaseDate, developerName, genreName, publisherName, null);
                                        games.add(game);
                                        if(jsonArray.length() == games.size()) {
                                            listener.OnResponse(games);
                                        }
                                    }

                                    @Override
                                    public void OnResponse(Object response) {
                                        Game game = new Game(scores,id, name, releaseDate, developerName, genreName, publisherName, (Bitmap) response);
                                        games.add(game);
                                        if(jsonArray.length() == games.size()) {
                                            listener.OnResponse(games);
                                        }
                                    }
                                });
                                if(games.size() == jsonArray.length()) {
                                    listener.OnResponse(games);
                                }
                            }
                        });
                    }

                } catch (JSONException jExcept) {
                    //Handle error
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                String body = new String(error.networkResponse.data);
                listener.OnError(error);
            }
        });
        ReqQ.getInstance(mContext).addToRequestQueue(getRequest);
    }

    //Score functions

    public void ChangeScore (String amount, final ResponseListener listener) {
        String url = "http://uniscoreapp-env.krgm9ru437.eu-west-1.elasticbeanstalk.com/Scores";
        Map<String,String> params = new HashMap<String, String>();
        params.put("token",LoggedInUser.getInstance().token);
        params.put("score",amount);
        JsonObjectRequest patchRequest = new JsonObjectRequest(Request.Method.PATCH, url, new JSONObject(params), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                listener.OnResponse(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                listener.OnError(error);
            }
        });

        ReqQ.getInstance(mContext).addToRequestQueue(patchRequest);
    }

    public void DeleteScore(String id ,final ResponseListener listener) {
        String url = String.format("http://uniscoreapp-env.krgm9ru437.eu-west-1.elasticbeanstalk.com/Scores?token=%s&id=%s", LoggedInUser.getInstance().token, id);

        StringRequest deleteReq = new StringRequest(Request.Method.DELETE, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                listener.OnResponse(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                String body = new String(error.networkResponse.data);
                listener.OnError(error);
            }
        });
        ReqQ.getInstance(mContext).addToRequestQueue(deleteReq);
    }

    public void GetScoresByUserId(String userId, final ResponseListener listener) {
        String url = String.format("http://uniscoreapp-env.krgm9ru437.eu-west-1.elasticbeanstalk.com/Scores?userId=%s", userId);

        JsonArrayRequest getRequest = new JsonArrayRequest(Request.Method.GET, url, null, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                try {

                    final List<Score> scores = new ArrayList<>();
                    if(LoadedResources.getInstance().scores != null)
                        LoadedResources.getInstance().scores.clear();
                    final JSONArray jsonArray = response;
                    if (response.length() == 0) {
                        listener.OnResponse(scores);
                        return;
                    }
                    for (int i = 0; i < response.length(); i++) {
                        final int scoreId = response.getJSONObject(i).getInt("Id");
                        final int userId = response.getJSONObject(i).getInt("UserId");
                        final int gameId = response.getJSONObject(i).getInt("GameId");
                        final int score = response.getJSONObject(i).getInt("Score");
                        String url = String.format("http://uniscoreapp-env.krgm9ru437.eu-west-1.elasticbeanstalk.com/Games?id=%s", gameId);
                        JsonArrayRequest getGameReq = new JsonArrayRequest(Request.Method.GET, url, null, new Response.Listener<JSONArray>() {
                            @Override
                            public void onResponse(JSONArray response) {
                                try {
                                    final String name = response.getJSONObject(0).getString("Name");
                                    final String developerName = response.getJSONObject(0).getString("DeveloperName");
                                    final String releaseDate = response.getJSONObject(0).getString("ReleaseDate");
                                    final String genreName = response.getJSONObject(0).getString("GenreName");
                                    final String publisherName = response.getJSONObject(0).getString("PublisherName");
                                    GetProfilePicture('G' + String.valueOf(gameId), new ResponseListener() {
                                        @Override
                                        public void OnError(VolleyError error) {

                                        }

                                        @Override
                                        public void OnResponse(Object response) {
                                            Game game = new Game(null, gameId, name, releaseDate, developerName, genreName, publisherName, (Bitmap) response);
                                            Score scoreObject = new Score(scoreId, userId, gameId, score, game, null);
                                            scores.add(scoreObject);

                                            if(jsonArray.length() == scores.size()) {
                                                listener.OnResponse(scores);
                                            }
                                        }
                                    });

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                String msg = new String(error.networkResponse.data);
                            }
                        });
                        ReqQ.getInstance(mContext).addToRequestQueue(getGameReq);
                    }
                } catch (JSONException jExcept) {

                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                listener.OnError(error);
            }
        });
        ReqQ.getInstance(mContext).addToRequestQueue(getRequest);
    }

    public void GetScoresByGameId(String gameId, final ResponseListener listener) {
        String url = String.format("http://uniscoreapp-env.krgm9ru437.eu-west-1.elasticbeanstalk.com/Scores?gameId=%s", gameId);

        JsonArrayRequest getRequest = new JsonArrayRequest(Request.Method.GET, url, null, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                try {

                    final List<Score> scores = new ArrayList<>();
                    if(LoadedResources.getInstance().scores != null)
                        LoadedResources.getInstance().scores.clear();
                    final JSONArray jsonArray = response;
                    if (response.length() == 0) {
                        listener.OnResponse(scores);
                        return;
                    }
                    for (int i = 0; i < response.length(); i++) {
                        final int scoreId = response.getJSONObject(i).getInt("Id");
                        final int userId = response.getJSONObject(i).getInt("UserId");
                        final int gameId = response.getJSONObject(i).getInt("GameId");
                        final int score = response.getJSONObject(i).getInt("Score");
                        String url = String.format("http://uniscoreapp-env.krgm9ru437.eu-west-1.elasticbeanstalk.com/Users?id=%s", userId);
                        JsonArrayRequest getUserReq = new JsonArrayRequest(Request.Method.GET, url, null, new Response.Listener<JSONArray>() {
                            @Override
                            public void onResponse(JSONArray response) {
                                try {
                                    final String username = response.getJSONObject(0).getString("Username");
                                    final String email = response.getJSONObject(0).getString("Email");
                                    GetProfilePicture('U' + String.valueOf(userId), new ResponseListener() {
                                        @Override
                                        public void OnError(VolleyError error) {

                                        }

                                        @Override
                                        public void OnResponse(Object response) {
                                            User user = new User(String.valueOf(userId), username, email, null, (Bitmap) response);
                                            Score scoreObject = new Score(scoreId, userId, gameId, score, null, user);
                                            scores.add(scoreObject);

                                            if(jsonArray.length() == scores.size()) {
                                                listener.OnResponse(scores);
                                            }
                                        }
                                    });

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                String msg = new String(error.networkResponse.data);
                            }
                        });
                        ReqQ.getInstance(mContext).addToRequestQueue(getUserReq);
                    }
                } catch (JSONException jExcept) {

                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                listener.OnError(error);
            }
        });
        ReqQ.getInstance(mContext).addToRequestQueue(getRequest);
    }

    public void PostFollower (User userToFollow, final ResponseListener listener) {
        String url = "http://uniscoreapp-env.krgm9ru437.eu-west-1.elasticbeanstalk.com/Followers";
        Map<String, String> params = new HashMap<String, String>();
        params.put("followedId", userToFollow.id);
        params.put("token", LoggedInUser.getInstance().token);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, new JSONObject(params), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                listener.OnResponse(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                String msg = new String(error.networkResponse.data);
                listener.OnError(error);
            }
        });
        ReqQ.getInstance(mContext).addToRequestQueue(jsonObjectRequest);
    }

    public void DeleteFollower (String userToUnfollowId, final ResponseListener listener) {
        String url = String.format("http://uniscoreapp-env.krgm9ru437.eu-west-1.elasticbeanstalk.com/Followers?followedId=%s&token=%s", userToUnfollowId, LoggedInUser.getInstance().token);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.DELETE, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                listener.OnResponse(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                listener.OnError(error);
            }
        });
        ReqQ.getInstance(mContext).addToRequestQueue(jsonObjectRequest);
    }

    public void GetFollowing(String followerId, final ResponseListener listener) {
        String url = String.format("http://uniscoreapp-env.krgm9ru437.eu-west-1.elasticbeanstalk.com/Followers?followerId=%s", followerId);

        JsonArrayRequest getRequest = new JsonArrayRequest(Request.Method.GET, url, null, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                try {
                    final List<User> following = new ArrayList<>();
                    final int responseLength = response.length();
                    if(responseLength == 0) {
                        listener.OnResponse(following);
                    }
                    for (int i = 0; i < response.length(); i++) {

                        final int followedId = response.getJSONObject(i).getInt("FollowedId");

                        GetUser(String.valueOf(followedId), new ResponseListener() {
                            @Override
                            public void OnError(VolleyError error) {

                            }

                            @Override
                            public void OnResponse(Object response) {
                                following.add(((List<User>) response).get(0));
                                if(responseLength == following.size()) {
                                    listener.OnResponse(following);
                                }
                            }
                        });
                    }

                } catch (JSONException jExcept) {

                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                listener.OnError(error);
            }
        });
        ReqQ.getInstance(mContext).addToRequestQueue(getRequest);
    }

    public void GetFollowingIds(String followerId, final ResponseListener listener) {
        String url = String.format("http://uniscoreapp-env.krgm9ru437.eu-west-1.elasticbeanstalk.com/Followers?followerId=%s", followerId);

        JsonArrayRequest getRequest = new JsonArrayRequest(Request.Method.GET, url, null, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                try {
                    final List<User> following = new ArrayList<>();
                    final int responseLength = response.length();
                    for (int i = 0; i < response.length(); i++) {

                        final int followedId = response.getJSONObject(i).getInt("FollowedId");
                        following.add(new User(String.valueOf(followedId), null, null, null, null));
                    }
                    listener.OnResponse(following);

                } catch (JSONException jExcept) {

                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                listener.OnError(error);
            }
        });
        ReqQ.getInstance(mContext).addToRequestQueue(getRequest);
    }

    public void GetFollowers (String followedId, final ResponseListener listener) {
        String url = String.format("http://uniscoreapp-env.krgm9ru437.eu-west-1.elasticbeanstalk.com/Followers?followedId=%s", followedId);

        JsonArrayRequest getRequest = new JsonArrayRequest(Request.Method.GET, url, null, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                try {
                    final List<User> followers = new ArrayList<>();
                    final int responseLength = response.length();
                    if(responseLength == 0) {
                        listener.OnResponse(followers);
                    }
                    for (int i = 0; i < response.length(); i++) {

                        int followerId = response.getJSONObject(i).getInt("FollowerId");

                        GetUser(String.valueOf(followerId), new ResponseListener() {
                            @Override
                            public void OnError(VolleyError error) {

                            }

                            @Override
                            public void OnResponse(Object response) {
                                followers.add(((List<User>) response).get(0));
                                if(responseLength == followers.size()) {
                                    listener.OnResponse(followers);
                                }
                            }
                        });
                    }
                } catch (JSONException jExcept) {

                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                listener.OnError(error);
            }
        });
        ReqQ.getInstance(mContext).addToRequestQueue(getRequest);
    }

    public void PostProfilePicture (final String imageString, final ResponseListener listener) {
        String url = "http://uniscoreapp-env.krgm9ru437.eu-west-1.elasticbeanstalk.com/ProfilePics";


        Map<String, String> params = new HashMap<String, String>();

        StringRequest postReq = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                listener.OnResponse(null);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                listener.OnError(null);
                String errormsg = new String(error.networkResponse.data);
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("fileStream", imageString);
                params.put("token", LoggedInUser.getInstance().token);
                return params;
            }
        };
        ReqQ.getInstance(mContext).addToRequestQueue(postReq);
    }

    public void GetProfilePicture (final String id, final ResponseListener listener) {
        String url = String.format("http://uniscoreapp-env.krgm9ru437.eu-west-1.elasticbeanstalk.com/ProfilePics?id=%s", id);

        ImageRequest imgReq = new ImageRequest(url, new Response.Listener<Bitmap>() {
            @Override
            public void onResponse(Bitmap response) {
                listener.OnResponse(response);
            }
        }, 128, 128, ImageView.ScaleType.FIT_CENTER, null, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                listener.OnResponse(null);
            }
        });
        ReqQ.getInstance(mContext).addToRequestQueue(imgReq);
    }
}